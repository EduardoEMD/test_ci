<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	
	<?php if (isset($message)): ?>
		<div>
			<h1 align="center"><?php echo $message; ?></h1>
		</div>
	<?php endif ?>

	<form action="<?= base_url("user/signup");  ?>" method="post" class="col-md-5">
		
		<label for="">Name</label><br>
		<input class="form-control" type="text" name="name"  value="<?= set_value('name'); ?>">		
		
		<?php if (!empty(form_error('name'))): ?>
			
			<div class="alert alert-danger">
				<?php echo form_error('name'); ?>
			</div>

		<?php endif ?>

		<label for="">Surname</label><br>
		<input class="form-control" type="text" name="surname"  value="<?= set_value('surname'); ?>">		
		<?php if (!empty(form_error('surname'))): ?>
			
			<div class="alert alert-danger">
				<?php echo form_error('surname'); ?>
			</div>

		<?php endif ?>

		<label for="">Age</label><br>
		<input class="form-control" type="text" name="age" value="<?= set_value('age'); ?>">	
		
		<?php if (!empty(form_error('age'))): ?>
			
			<div class="alert alert-danger">
				<?php echo form_error('age'); ?>
			</div>

		<?php endif ?>

		<label for="">Email</label><br>
		<input class="form-control" type="text" name="email"  value="<?= set_value('email'); ?>">	

		<?php if (!empty(form_error('email'))): ?>
			
			<div class="alert alert-danger">
				<?php echo form_error('email'); ?>
			</div>

		<?php endif ?>
		
		<label for="">password</label><br>
		<input class="form-control" type="password" name="password"  value="<?= set_value('password'); ?>">
		<?php if (!empty(form_error('password'))): ?>
			
			<div class="alert alert-danger">
				<?php echo form_error('password'); ?>
			</div>

		<?php endif ?>
		
		<?php if (isset($inviter)): ?>
			<input class="form-control" type="text" readonly value="<?= $inviter ?>">
		<?php endif ?>

		<br>
		<button class="btn btn-success">Send</button>	

	</form>

</body>
</html>