<?php 
defined('BASEPATH') or exit('No direct script access allowed');
/**
* 
*/
class Users_model extends CI_Model
{
	private $table; 

	function __construct()
	{
		parent::__construct();

		$this->table = 'users';
	}

	function add_user($referer = null){

		$status   = empty($referer) ? 0 : 1;
		$name  	  = $this->input->post('name');
		$surname  = $this->input->post('surname');
		$age 	  = $this->input->post('age');
		$email    = $this->input->post('email');
		$password = $this->input->post('password');

		$user = [
			'name'     => $name,
			'surname'  => $surname,
			'age' 	   => $age,
			'email'    => $email,
			'password' => password_hash($password,PASSWORD_DEFAULT),
			'status'   => $status
		];

		$this->db->insert($this->table, $user);

		return $this->db->insert_id();
	}

	function activate($id,$key){

		$finded = $this->db->get_where($this->table, ['id' => $id])->first_row();

		if(md5($finded->email.$finded->id) == $key){

			$this->db->set('status',1);
			$this->db->where('id',$finded->id);
			$this->db->update($this->table);

			return true;

		}else{
			
			return false;

		}

	}
}

?>
