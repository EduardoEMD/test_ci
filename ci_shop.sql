/*
Navicat MySQL Data Transfer

Source Server         : Eduard
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : ci_shop

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-11-15 23:50:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `asd` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('28', 'Eduard', 'Hovhannisyan', '123', 'edhovhannisyan97@gmail.com', '$2y$10$QKeOQtQRvu7So1v4Tu0Sx..3geTPLEO7XXPRfjF1OsRcF0/ftHPHm', '1');
